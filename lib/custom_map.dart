import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_google_places_api/flutter_google_places_api.dart';
import 'package:flutter_google_places_api/requests/find_place_request.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:permission_handler/permission_handler.dart';

class CustomMap extends StatefulWidget {
  @override
  _CustomMapState createState() => _CustomMapState();
}

class _CustomMapState extends State<CustomMap> {
  GoogleMapController _mapController;
  Timer _debouncer;
  List<TextSearchResult> results = [];
  TextEditingController _textEditingController = TextEditingController();

  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  Set<Polyline> _polylines = {};
  @override
  void initState() {
    super.initState();
    _requestPermissions();
    _polylines.add(Polyline(polylineId: PolylineId("123"),points: [LatLng(-12.186064601362995, -77.0078995662669),LatLng(-12.172892494723186, -77.00583962975495),LatLng(-12.176751905760165, -77.0086720424589),]));
    _textEditingController.addListener(_onChanged);
  }

  @override
  void dispose() {
    _textEditingController?.removeListener(_onChanged);
    _textEditingController?.dispose();
    _debouncer?.cancel();
    super.dispose();
  }

  void _onMapCreated(GoogleMapController mapController) {
    _mapController = mapController;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          _mapController.moveCamera(CameraUpdate.newLatLng(
              LatLng(37.780340509303684, -122.43313964131856)));
        },
      ),
      body: Stack(
        children: [
          GoogleMap(
            indoorViewEnabled: true,
            polylines: _polylines,
            myLocationButtonEnabled: true,
            myLocationEnabled: true,
            zoomControlsEnabled: true,
            onMapCreated: _onMapCreated,
            markers: Set<Marker>.of(markers.values),
            initialCameraPosition: CameraPosition(
              zoom: 15,
              target: LatLng(-12.048564260579854, -76.97115980631965),
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              height: 346,
              padding: EdgeInsets.symmetric(horizontal: 4, vertical: 12),
              width: MediaQuery.of(context).size.width * 0.8,
              child: Column(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                    ),
                    padding: EdgeInsets.only(top: 20, left: 4, right: 4),
                    height: 40,
                    child: TextField(
                      controller: _textEditingController,
                    ),
                  ),
                  if (results.isNotEmpty)
                    Container(
                      color: Colors.white,
                      child: ListView.separated(
                        shrinkWrap: true,
                        itemCount: results.length,
                        separatorBuilder: (context, index) {
                          return Divider();
                        },
                        itemBuilder: (context, index) {
                          return ListTile(
                            onTap: () {
                              final result = results[index];
                              _addIfExists(result);
                              setState(() {
                                _textEditingController.text =
                                    result.formattedAddress;
                                results = [];
                              });
                              _mapController.animateCamera(
                                  CameraUpdate.newLatLng(LatLng(
                                      result.geometry.location.lat,
                                      result.geometry.location.lng)));
                            },
                            title: Text(results[index].formattedAddress),
                          );
                        },
                      ),
                    )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _onChanged() {
    setState(() {
      results = [];
    });
    if (_textEditingController.text.length < 1) {
      _debouncer?.cancel();
      return;
    }
    if (_debouncer?.isActive ?? false) {
      _debouncer?.cancel();
    }
    _debouncer = Timer(Duration(milliseconds: 700), () {
      callPlaces(_textEditingController.text);
    });
  }

  void callPlaces(String location) async {
    final response = await TextSearchRequest(
      key: 'AIzaSyCcaCFRPSJQFj736rjIGdGt99zhPBfMlQA',
      query: location,
    ).call();
    results = response.results.length > 5
        ? response.results.sublist(0, 5)
        : response.results.sublist(0, response.results.length);
    print(results.length);
    multilineprint(results.toString());
    setState(() {});
  }

  void multilineprint(String string) {
    final pattern = RegExp('.{1,800}');
    pattern.allMatches(string).forEach((match) {
      print(match.group(0));
    });
  }

  void _requestPermissions() async {
    if (await Permission.location.request().isGranted) {
      print("Tenemos permisos");
    }
  }

  void _addIfExists(TextSearchResult result) {
    print(result);
    final markerId = MarkerId(result.placeId);
    if (!markers.containsKey(markerId)) {
      final marker = Marker(
          markerId: markerId,
          position: LatLng(
              result.geometry.location.lat, result.geometry.location.lng),
          infoWindow: InfoWindow(
              title: result.formattedAddress,
              snippet: DateTime.now().toString()),
          onTap: () {
            print(result);
          });
      setState(() {
        markers[markerId] = marker;
      });
    }
  }
}
